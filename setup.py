from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='tornado-boilerplate',
    version='0.1.0',
    description='Tornado boilerplate program',
    long_description=readme,
    author='Stefan Midjich',
    author_email='swehack@gmail.com',
    url='https://gitlab.com/stemid/tornado-boilerplate',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
