# Tornado boilerplate

Example project boilerplate for creating an async daemon program with Tornado.

## Rabbitmq

For dev purposes;

    $ podman run --name rabbitmq -p 5672:5672 -e RABBITMQ_DEFAULT_VHOST=konsument rabbitmq:3-management

## Install dependencies & Run

    $ python3 -m venv .venv
    $ source .venv/bin/activate
    $ pip install -r requirements.txt
    $ python -m konsument.server --debug --amqp-url amqp://guest:guest@localhost:5672/konsument
    2021-01-10 00:09:54,643 - konsument - INFO: Starting server
    2021-01-10 00:09:54,643 - konsument - INFO: Connecting to amqp://guest:guest@localhost:5672/konsument

## Produce messages

    $ python producer.py --amqp-url amqp://guest:guest@localhost:5672/konsument

