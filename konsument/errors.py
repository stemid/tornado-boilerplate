

class CommandRunError(Exception):
    pass

class CommandInitError(Exception):
    pass

class GamePlayerAlreadyJoined(Exception):
    pass
