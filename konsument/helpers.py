import pkgutil


def iter_namespace(ns):
    """Helper function to iterate over modules in a specific namespace"""
    return pkgutil.iter_modules(ns.__path__, '{name}.'.format(
        name=ns.__name__
    ))

