import logging
import sys

class ApplicationLog(object):

    def __init__(self, app_name, **kw):
        debug = kw.get('debug', False)
        log_format = kw.get(
            'log_format',
            '%(asctime)s - %(name)s - %(levelname)s: %(message)s'
        )
        log = logging.getLogger(app_name)

        # Get all the tornado loggers
        tornado_application = logging.getLogger('tornado.application')
        tornado_access = logging.getLogger('tornado.access')
        tornado_general = logging.getLogger('tornado.general')

        h = logging.StreamHandler(sys.stdout)

        if debug:
            h.setLevel(logging.DEBUG)
            log.setLevel(logging.DEBUG)
        else:
            h.setLevel(logging.INFO)
            log.setLevel(logging.INFO)

        formatter = logging.Formatter(log_format)
        h.setFormatter(formatter)
        log.addHandler(h)

        # Add our handler to all the tornado loggers
        tornado_application.addHandler(h)
        tornado_access.addHandler(h)
        tornado_general.addHandler(h)

        self.logger = log
