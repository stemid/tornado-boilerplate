import functools
from os import environ
from sys import exit

import click
import tornado.autoreload
from tornado.ioloop import IOLoop

from konsument import APP_NAME
from konsument.log import ApplicationLog
from konsument.consumers import PikaConsumer


@click.command()
@click.option(
    '--amqp-url',
    default=lambda: environ.get('AMQP_URL', False),
    help='RabbitMQ connection URI'
)
@click.option(
    '--debug',
    default=False,
    is_flag=True,
    help='Run in development mode with autoreload'
)
def run_server(amqp_url, debug):
    """
    Start the server.

    Parameters
    ----------
    amqp_url : str
        RabbitMQ connection URI
    debug : bool
        Enable autoreload and debug output
    """

    l = ApplicationLog(APP_NAME, debug=debug).logger

    # Set to use asyncio
    IOLoop.configure('tornado.platform.asyncio.AsyncIOLoop')

    io_loop = IOLoop.current()
    pc = PikaConsumer(io_loop, amqp_url, l)

    # Run callback at startup
    #init_frontend_cb = functools.partial(mf.init_frontend)
    #io_loop.add_callback(init_frontend_cb)

    # Run callback periodically every X ms
    #periodic_cb = tornado.ioloop.PeriodicCallback(
    #    callback=mf.periodic_callback,
    #    callback_time=5000
    #)
    #periodic_cb.start()

    if debug:
        # More verbose reload message
        tornado.autoreload.add_reload_hook(
            lambda: l.info('Code changed, reloading server')
        )
        # Run callback at reload
        #tornado.autoreload.add_reload_hook(mf.setup_streams)
        tornado.autoreload.start()

    # Start main infinite loop
    try:
        l.info('Starting server')
        #io_loop.start()
        pc.run()
    except KeyboardInterrupt as e:
        pass


if __name__ == '__main__':
    exit(run_server(None))

