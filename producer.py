# -*- coding: utf-8 -*-
# Based on example: https://github.com/pika/pika/blob/master/examples/producer.py

import json
import random
from sys import exit
from os import environ

import click
import pika


def getticker():
    tickers = {
        'MXSE.EQBR.LKOH': (1933, 1940),
        'MXSE.EQBR.MSNG': (1.35, 1.45),
        'MXSE.EQBR.SBER': (90, 92),
        'MXSE.EQNE.GAZP': (156, 162),
        'MXSE.EQNE.PLZL': (1025, 1040),
        'MXSE.EQNL.VTBR': (0.05, 0.06)
    }

    return list(tickers.keys())[random.randrange(0, len(tickers) - 1)]


@click.command()
@click.option(
    '--amqp-url',
    default=lambda: environ.get('AMQP_URL', False),
    required=True,
    help='RabbitMQ connection URI'
)
def run_producer(amqp_url):
    connection = pika.BlockingConnection(
        pika.URLParameters(amqp_url)
    )
    main_channel = connection.channel()

    main_channel.exchange_declare(
        exchange='message',
        exchange_type='topic'
    )


    _COUNT_ = 10

    for i in range(0, _COUNT_):
        ticker = getticker()
        msg = {
            'order.stop.create': {
                'data': {
                    'params': {
                        'condition': {
                            'ticker': ticker
                        }
                    }
                }
            }
        }
        main_channel.basic_publish(
            exchange='message',
            routing_key='example.text',
            body=json.dumps(msg),
            properties=pika.BasicProperties(content_type='application/json')
        )
        print('send ticker %s' % ticker)

    connection.close()


if __name__ == '__main__':
    exit(run_producer(None))
